package test.com.slice.mathfactory;

import main.com.slice.mathOperations.MathFactory;
import main.com.slice.mathOperations.cache.MathFactoryCache;
import main.com.slice.mathOperations.implement.MathDivision;
import main.com.slice.mathOperations.implement.MathMinus;
import main.com.slice.mathOperations.implement.MathMultiply;
import main.com.slice.mathOperations.implement.MathPlus;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MathFactoryCacheTest {

    MathPlus mathPlus = new MathPlus();
    MathMinus mathMinus = new MathMinus();
    MathMultiply mathMultiply = new MathMultiply();
    MathDivision mathDivision = new MathDivision();

    MathFactory[] mathFactory = new MathFactory[]{mathPlus, mathMinus, mathMultiply, mathDivision};

    MathFactoryCache cut = new MathFactoryCache(mathFactory);


    static Arguments[] mathFactoryCacheTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3.333333, "/", 10, 3),
                Arguments.arguments(20, "+", 15, 5),
                Arguments.arguments(4, "-", 10, 6),
                Arguments.arguments(150, "*", 10, 15),
                Arguments.arguments(0, "del", 22, 12)

        };
    }

    @ParameterizedTest
    @MethodSource("mathFactoryCacheTestArgs")
    void mathFactoryCacheTest(double expected, String operation, double number1, double number2){
        double actual = cut.getFactory(operation, number1, number2);
        Assertions.assertEquals(expected, actual);
    }

}
