package test.com.slice.phonenumber;

import main.com.slice.phonenumber.PhoneNumberValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PhoneNumberText {

    PhoneNumberValidator cut = new PhoneNumberValidator();

    static Arguments[] checkPhoneNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments("+380 71 8021-70-71", true),
                Arguments.arguments("+1 312 345-34-94", true),
                Arguments.arguments("+1 212 212-12-21", true),
                Arguments.arguments("+1 132 ddd-34-94", false),
                Arguments.arguments("+1 212 12-112-12", false),
                Arguments.arguments("+080 212 222-12-12", false)
        };
    }
    @ParameterizedTest
    @MethodSource("checkPhoneNumberTestArgs")
    void generateRandomNumberTest(String str,boolean expected){
        boolean actual=cut.checkPhoneNumber(str);
        Assertions.assertEquals(expected, actual);
    }
}
