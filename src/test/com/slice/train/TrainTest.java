package test.com.slice.train;

import main.com.slice.train.models.Train;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TrainTest {


    @Test
    void toStringTest(){
        Train cut = new Train("Dnepr", "222i", 17.30, 48);
        String  actual = cut.toString();
        String expected = "Train{destination='Dnepr', numberOfTrain='222i', time=17.3, countOfPlace=48}";
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] equalsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, new Train("Dnepr", "222i", 17.30, 48), new Train("Dnepr", "222i", 17.30, 48)),
                Arguments.arguments(false, new Train("Dnipro", "222i", 17.30, 48), new Train("Dnepr", "222i", 17.30, 48)),
                Arguments.arguments(false, new Train("Dnepr", "321y", 17.30, 48), new Train("Dnepr", "222i", 17.30, 48)),
                Arguments.arguments(false, new Train("Dnepr", "222i", 18.30, 48), new Train("Dnepr", "222i", 17.30, 48)),
                Arguments.arguments(false, new Train("Dnepr", "222i", 17.30, 30), new Train("Dnepr", "222i", 17.30, 48)),
        };
    }

    @ParameterizedTest
    @MethodSource("equalsTestArgs")
    void equalsTest(boolean expected, Train train1, Train train2){
        boolean actual = train1.equals(train2);
        Assertions.assertEquals(expected, actual);
    }

}
