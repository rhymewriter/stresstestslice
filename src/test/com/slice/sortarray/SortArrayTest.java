package test.com.slice.sortarray;

import main.com.slice.sortarray.SortArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SortArrayTest {

    SortArray cut = new SortArray();

    static Arguments[] sortTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{16,17,18,19,20}, new int[]{18,16,20,19,17}),
                Arguments.arguments(new int[]{1,2,3,4,5}, new int[]{4,5,3,2,1}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortBubble(int[] expected, int[] array){
        int[] actual = cut.sortArray(array);
        Assertions.assertArrayEquals(expected,actual);
    }

}
