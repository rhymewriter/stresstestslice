package test.com.slice.regexnumber;

import main.com.slice.regexhexnumber.RegexHexNumber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class RegexHexNumberTest {

    RegexHexNumber cut = new RegexHexNumber();

    static Arguments[] RegexHexNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, "2B88"),
                Arguments.arguments(true, "0000000000000"),
                Arguments.arguments(true, "aF2"),
                Arguments.arguments(false, "222kdv"),
                Arguments.arguments(false, "AAABB")


        };
    }

    @ParameterizedTest
    @MethodSource("RegexHexNumberTestArgs")
    void RegexHexNumberTest(boolean expected, String hexNumber){
        boolean actual = cut.checkHexNumber(hexNumber);
        Assertions.assertEquals(expected, actual);
    }


}
