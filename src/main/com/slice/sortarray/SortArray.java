package main.com.slice.sortarray;

public class SortArray {

    public int[] sortArray(int[] array) {
        boolean needIteration = true;
        while(needIteration){
            needIteration = false;
            for (int i = 1; i < array.length; i++){
                if (array[i] < array[i-1]){
                    swap(array, i, i-1);
                    needIteration = true;
                }
            }
        }
        return array;
    }

    private void swap (int[] a, int index1, int index2) {
        int tmp = a[index1];
        a[index1] = a[index2];
        a[index2] = tmp;
    }

}
