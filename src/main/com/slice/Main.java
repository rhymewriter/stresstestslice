package main.com.slice;

import main.com.slice.mathOperations.MathFactory;
import main.com.slice.mathOperations.cache.MathFactoryCache;
import main.com.slice.mathOperations.implement.MathDivision;
import main.com.slice.mathOperations.implement.MathMinus;
import main.com.slice.mathOperations.implement.MathMultiply;
import main.com.slice.mathOperations.implement.MathPlus;
import main.com.slice.train.models.Train;
import main.com.slice.train.services.TrainComparator;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Train train1 = new Train("Kyiv", "225D", 12.30, 48);
        Train train2 = new Train("Odessa", "144n", 8.10, 38);
        Train train3 = new Train("Dnepr", "226D", 17.10, 48);
        Train train4 = new Train("Luck", "100", 13.30, 38);//true
        Train train5 = new Train("Chernigov", "117K", 15.00, 48);
        Train train6 = new Train("Uzhgorod", "45K", 9.10, 28);
        Train train7 = new Train("Dnepr", "10P", 14.20, 48);


        List<Train> trains = Arrays.asList(train1, train2, train3, train4, train5, train6, train7);

        TrainComparator comparator = new TrainComparator();

        trains.sort(comparator);

        for (Train t: trains) {
            System.out.println(t);
        }

        MathPlus mathPlus = new MathPlus();
        MathMinus mathMinus = new MathMinus();
        MathMultiply mathMultiply = new MathMultiply();
        MathDivision mathDivision = new MathDivision();

        MathFactory[] mathFactory = new MathFactory[]{mathPlus, mathMinus, mathMultiply, mathDivision};

        MathFactoryCache mathFactoryCache = new MathFactoryCache(mathFactory);

        double h = mathFactoryCache.getFactory("division", 10, 3);

        System.out.println(h);


    }
}
