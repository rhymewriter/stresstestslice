package main.com.slice.mathOperations.cache;

import main.com.slice.mathOperations.MathFactory;

public class MathFactoryCache {

    private MathFactory[] mathFactories;


    public MathFactoryCache(MathFactory[] mathFactories) {
        this.mathFactories = mathFactories;
    }

    public double getFactory(String operation, double number1, double number2){

        switch (operation)
        {
            case "+":
                return mathFactories[0].countTwoValues(number1, number2);
            case "-":
                return mathFactories[1].countTwoValues(number1, number2);
            case "*":
                return mathFactories[2].countTwoValues(number1, number2);
            case "/":
                return mathFactories[3].countTwoValues(number1, number2);
            default:
                System.out.println("Invalid command");
                return 0;

        }
    }
}
