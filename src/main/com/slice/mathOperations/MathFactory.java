package main.com.slice.mathOperations;

public interface MathFactory {

    double countTwoValues(double number1, double number2);

}
