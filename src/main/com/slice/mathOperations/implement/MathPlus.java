package main.com.slice.mathOperations.implement;

import main.com.slice.mathOperations.MathFactory;

public class MathPlus implements MathFactory {
    @Override
    public double countTwoValues(double number1, double number2) {
        return number1 + number2;
    }
}
