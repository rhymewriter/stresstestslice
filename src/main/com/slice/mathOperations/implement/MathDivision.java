package main.com.slice.mathOperations.implement;

import main.com.slice.mathOperations.MathFactory;

public class MathDivision implements MathFactory {
    @Override
    public double countTwoValues(double number1, double number2) {
        double result = (number1 / number2)*1000_000;
        result = Math.round(result);
        result = result/1000_000;
        return result;
    }
}
