package main.com.slice.train.services;

import main.com.slice.train.models.Train;

import java.util.Comparator;

public class TrainComparator implements Comparator<Train> {


    @Override
    public int compare(Train obj1, Train obj2) {

        int result = obj1.getCountOfPlace()-obj2.getCountOfPlace();

        if(result == 0){
            result = obj1.getDestination().compareTo(obj2.getDestination());
            if(result==0){

                result = (int) ((obj1.getTime()-obj2.getTime())*100);
            }
        }

        return result;
    }
}
